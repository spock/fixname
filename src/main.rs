extern crate clap;
extern crate glob;
extern crate regex;

use std::env;

//use glob::glob;
use clap::{App, Arg};
use regex::Regex;
use std::fs;
use std::process;

use std::{
    fs::File,
    io::{prelude::*, BufReader},
    path::Path,
};

const APP_VERSION: &'static str = env!("CARGO_PKG_VERSION");
const APP_NAME: &'static str = env!("CARGO_PKG_NAME");
const APP_AUTHOR: &'static str = env!("CARGO_PKG_AUTHORS");

fn lines_from_file(filename: impl AsRef<Path>) -> Vec<String> {
    let file = match File::open(filename) {
        Ok(f) => f,
        Err(e) => {
            println!("{}", e);
            process::exit(-1)
        }
    };
    let buf = BufReader::new(file);
    buf.lines().map(|l| l.expect("Can't read line")).collect()
}

fn main() {
    let default_filter_path = Path::new(&std::env::var("HOME").unwrap_or(String::from(".")))
        .join(".local")
        .join("fixname")
        .join("filter");
    let matches = App::new(APP_NAME)
        .version(APP_VERSION)
        .author(APP_AUTHOR)
        .about("Strip torrent downloaded movies/show names from spam")
        .arg(
            Arg::with_name("test")
                .short("t")
                .long("test")
                .takes_value(false)
                .help("Just test - not file changes"),
        )
        .arg(
            Arg::with_name("filter")
                .short("f")
                .takes_value(true)
                .help("Path to file with chars to filter")
                .default_value(default_filter_path.as_path().to_str().unwrap()),
        )
        .arg(
            Arg::with_name("files")
                .required(true)
                .multiple(true)
                .help("File list for rename"),
        )
        .get_matches();

    //let filters = vec!["WEB-DL","WEB", "-TBS", "[eztv]", "h264","x264", "REPACK", "-[YTS.AG]"];
    let filters: Vec<String> = lines_from_file(matches.value_of("filter").unwrap());
    let args: Vec<String> = env::args().collect();
    let dotr = Regex::new(r"\.+").unwrap();
    if args.len() < 2 {
        println!("Usage {}: filname/pattern", args[0]);
        process::exit(-1);
    }
    let files: Vec<_> = matches.values_of("files").unwrap().collect();
    for file in files.into_iter() {
        let full_path = Path::new(file.clone());
        let orig_name: String = full_path
            .file_name()
            .unwrap()
            .to_owned()
            .into_string()
            .unwrap();
        let mut t: String = orig_name.to_owned();
        for f in filters.iter() {
            t = t.replace(f, "");
        }
        let t: String = Path::new(&dotr.replace_all(&t, ".").into_owned())
            .to_str()
            .unwrap()
            .to_owned();
        if orig_name == t {
            println!("Nothing to do for: {}", full_path.to_str().unwrap());
        } else {
            let src = full_path.parent().unwrap().join(&orig_name);
            let dst = full_path.parent().unwrap().join(&t);
            if !matches.is_present("test") {
                match fs::rename(src.clone(), dst.clone()) {
                    Ok(_) => println!(
                        "Renamed {} -> {}",
                        src.as_path().to_str().unwrap(),
                        dst.as_path().to_str().unwrap()
                    ),
                    Err(e) => println!("Erorr renaming {}: {}", src.as_path().to_str().unwrap(), e),
                }
            } else {
                println!(
                    "TEST Renamed: {} -> {}",
                    src.as_path().to_str().unwrap(),
                    dst.as_path().to_str().unwrap()
                )
            }
        }
    }
}
